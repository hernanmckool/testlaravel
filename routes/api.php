<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('user', 'UserController');
Route::resource('service', 'ServiceController');
Route::post('subscriptions/add', 'SubscriptionController@add')->name('add_product');
Route::post('subscriptions/cancel', 'SubscriptionController@cancel')->name('cancel_product');