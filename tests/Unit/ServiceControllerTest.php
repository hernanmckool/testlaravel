<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ServiceControllerTest extends TestCase
{
	/**
	 * test store.
	 *
	 * @return void
	 */
	public function testStoreOk()
	{
		$response = $this->post('api/service' , [
			'service' => 'fake_service'
		]);
		$response->assertStatus(201)
			->assertJson([
				'service' => 'fake_service',
		]);
	}

	public function testStoreInvalidParams()
	{
		$response = $this->post('api/service' , []);
		$response->assertStatus(422);
	}
}
