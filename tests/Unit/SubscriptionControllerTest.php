<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubscriptionControllerTest extends TestCase
{
	/**
	 * test add.
	 *
	 * @return void
	 */
	public function testAddOk()
	{
		$response = $this->post('api/subscriptions/add' , [
			'service_id' => '1',
			'client_id' => '1',
		]);

		$response->assertStatus(201)
			->assertJson([
				'service_id' => '1',
				'client_id' => '1',
				'active' 	=> 1,
		]);
	}

	/**
	 * test add Invalid Params.
	 *
	 * @return void
	 */
	public function testAddInvalidParams()
	{
		$response = $this->post('api/subscriptions/add' , []);
		$response->assertStatus(422);
	}

	/**
	 * test add.
	 *
	 * @return void
	 */
	public function testACancelOk()
	{
		$response = $this->post('api/subscriptions/cancel' , [
			'service_id' => '1',
			'client_id' => '1',
		]);

		$response->assertStatus(200)
			->assertJson([
				'code' => 0,
		]);
	}

	/**
	 * test add Invalid Params.
	 *
	 * @return void
	 */
	public function testCancelInvalidParams()
	{
		$response = $this->post('api/subscriptions/cancel' , []);
		$response->assertStatus(422);
	}
}
