<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserControllerTest extends TestCase
{
	/**
	 * test store.
	 *
	 * @return void
	 */
	public function testStoreOk()
	{
		$response = $this->post('api/user' , [
			'name' => 'fake_name',
			'email' => 'fake_email@gmail.com',
		]);

		$response->assertStatus(201)
			->assertJson([
				'name' => 'fake_name',
				'email' => 'fake_email@gmail.com',
		]);
	}

	public function testStoreInvalidParams()
	{
		$response = $this->post('api/user' , []);
		$response->assertStatus(422);
	}
}

