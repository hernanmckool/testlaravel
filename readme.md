<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## TEST BENNU

**INSTALLATION:**
```
    - create database in mysql (DBname: testLaravel, user:root, password:root) check file .env
    - git clone https://gitlab.com/hernanmckool/testlaravel.git .
    - composer install
    - php artisan migrate
    - php artisan serve
```

**API REFERENCE**

**Note:** change the {{server}} variable to endpoint in use (Example: http://127.0.0.1:8000).

`POST` **ADD USER**
```
curl -X POST \
  {{server}}/api/user \
  -F 'name=Custom User' \
  -F email=custom_email@gmail.com

```

`POST` **ADD SERVICE**
```
curl -X POST \
  {{server}}/api/service \
  -F 'service=Custom Service'

```

`POST` **ADD SUBSCRIPTION**
```
curl -X POST \
  {{server}}/api/subscriptions/add \
  -F service_id=4 \
  -F client_id=1

```

`POST` **CANCEL SUBSCRIPTION**
```
curl -X POST \
  {{server}}/api/subscriptions/cancel \
  -F service_id=4 \
  -F client_id=1

```

`CONSOLE ARTISAN` **COUNT ACTIVE, CANCEL AND ALL SUBSCRIPTION BY DATE**
```
php artisan command:subscriptions
    >Insert date (YYYY-MM-DD):
    >2019-05-31
    
    -- console show:
    ------------------------------------------------------
    New Subscriptions at 2019-05-31: 5
    ------------------------------------------------------
    Canceled Subscriptions at 2019-05-31: 2
    ------------------------------------------------------
    Active Subscriptions at 2019-05-31: 3
    ------------------------------------------------------
```

`UNIT TEST`
```
Check the unit tests with the following command:
vendor/phpunit/phpunit/phpunit
FILES:
    - tests/Unit/ServiceControllerTest.php
    - tests/Unit/SubscriptionControllerTest.php
    - tests/Unit/UserControllerTest.php
```