<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Subscription;

class ShowSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:subscriptions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show new subscriptions, cancel subscriptions and all actives subscriptions by date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = $this->ask('Insert date (YYYY-MM-DD)');

        $newSubscriptions = Subscription::whereDate('created_at', $date)->count();

        $activeSubscriptions = Subscription::whereDate('created_at', $date)
                            ->where('active', 1)
                            ->count();

        $canceledSubscriptions = Subscription::whereDate('cancel_date', $date)
                            ->where('active', 0)
                            ->count();

        $this->line('------------------------------------------------------');
        $this->line('New Subscriptions at '.$date.': ' . $newSubscriptions);
        $this->line('------------------------------------------------------');
        $this->line('Canceled Subscriptions at '.$date.': ' . $canceledSubscriptions);
        $this->line('------------------------------------------------------');
        $this->line('Active Subscriptions at '.$date.': ' . $activeSubscriptions);
        $this->line('------------------------------------------------------');
    }
}