<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestSubscription;
use App\Subscription;
use Illuminate\Support\Carbon;

class SubscriptionController extends Controller
{
	/**
	 * @route api/subscriptions/add
	 * @param RequestSubscription $request
	 * @return mixed
	 */
	public function add(RequestSubscription $request)
	{
		$request->validated();
		$subscription = Subscription::create([
			'service_id' => $request->get('service_id'),
			'client_id'  => $request->get('client_id'),
			'active' 	 => Subscription::STATUS_ACTIVE,
		]);

		return response()->json($subscription, 201);
	}

	/**
	 * @route api/subscriptions/cancel
	 * @param RequestSubscription $request
	 * @return mixed
	 */
	public function cancel(RequestSubscription $request)
	{
		$request->validated();
		$subscription = Subscription::where('client_id', $request->get('client_id'))
					->where('service_id', $request->get('service_id'))
					->update(['active' => Subscription::STATUS_CANCEL, 'cancel_date' => Carbon::now()]);

		return response()->json(['code' => 0], 200);
	}
}
