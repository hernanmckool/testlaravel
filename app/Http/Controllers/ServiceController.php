<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestService;
use App\Exceptions\AppException;
use App\Service;

class ServiceController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  RequestService  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(RequestService $request)
	{
		$request->validated();
		$service = Service::create($request->post());

		return response()->json($service, 201);
	}
}