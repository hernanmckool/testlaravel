<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestUser;
use App\User;

class UserController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  RequestUser  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestUser $request)
    {
        $request->validated();
        $user = User::create($request->post());

        return response()->json($user, 201);
    }
}