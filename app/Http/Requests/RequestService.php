<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestService extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'service.required' => 'The :attribute field is required',
        ];
    }
}
